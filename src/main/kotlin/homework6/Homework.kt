package homework6

abstract class Animal() {
    var fullness = 0
    abstract val name: String
    abstract val height: Int
    abstract val weight: Int
    protected abstract val foodPreferences: Array<String>
    fun eat(food: String) {
        if (food in foodPreferences) fullness++
        if (fullness > 0) {
            println("$name наелся")
        } else {
            println("$name голоден")
        }
    }
}

class Lion(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("meat", "fish")
}

class Tiger(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("meat", "fish")
}

class Hippo(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("grass", "fruit", "vegetables")
}

class Wolf(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("meat", "carrion")
}

class Giraffe(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("grass", "fruit")
}

class Elephant(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("grass", "fruit")
}

class Chimpanzee(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("vegetables", "fruit")
}

class Gorilla(override val name: String, override val height: Int, override val weight: Int) :
    Animal() {
    override val foodPreferences = arrayOf("vegetables", "fruit")
}

fun feedAnimals(animals: Array<Animal>, food: Array<String>) {
    for (item in food) {
        for (animal in animals) {
            animal.eat(item)
        }
        println()
    }
}

fun main() {
    val lion = Lion("Zubo", 2, 50)
    val tiger = Tiger("Drako", 2, 79)
    val hippo = Hippo("Djoni", 3, 120)
    val giraffe = Giraffe("Bobi", 3, 56)
    val wolf = Wolf("Chernish", 1, 50)
    val elephant = Elephant("Nosatik", 1, 70)
    val chimpanzee = Chimpanzee("Kristi", 1, 40)
    val gorilla = Gorilla("Lucee", 2, 90)
    val animals = arrayOf(lion, tiger, hippo, giraffe, wolf, elephant, chimpanzee, gorilla)
    val foodArray = arrayOf("meat", "fruit")
    val food = "meat"

    lion.eat(food)
    tiger.eat(food)
    hippo.eat(food)
    giraffe.eat(food)
    wolf.eat(food)
    elephant.eat(food)
    chimpanzee.eat(food)
    gorilla.eat(food)
    println()
    feedAnimals(animals, foodArray)
}