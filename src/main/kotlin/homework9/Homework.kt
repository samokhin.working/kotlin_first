package homework9

data class Coordinates(val row: Int, val column: Int) {
}

abstract class Ship(open val coordinates: List<Coordinates>) {
    abstract val size: Int
    abstract var availableSizeOfShip: Int
    abstract fun checkShipIsAlive(): Boolean
    abstract fun containsRowAndColumn(coordinat: Coordinates): Boolean
    abstract fun decAvailableSizeOfShip()
}

class OneShip(override val coordinates: List<Coordinates>) : Ship(coordinates) {
    override val size = 1
    override var availableSizeOfShip = size
    override fun checkShipIsAlive(): Boolean = availableSizeOfShip != 0
    override fun containsRowAndColumn(coordinat: Coordinates): Boolean {
        return coordinat in coordinates
    }

    override fun decAvailableSizeOfShip() {
        availableSizeOfShip--
    }
}

class TwoShip(override val coordinates: List<Coordinates>) : Ship(coordinates) {
    override val size = 2
    override var availableSizeOfShip = size
    override fun checkShipIsAlive(): Boolean = availableSizeOfShip != 0
    override fun containsRowAndColumn(coordinat: Coordinates): Boolean {
        return coordinat in coordinates
    }

    override fun decAvailableSizeOfShip() {
        availableSizeOfShip--
    }

}

class Player(val name: String, val amountOfShip: List<Ship>) {
    var selfSeaField: Array<Array<Int>> = Array(4) { Array(4) { 0 } }
    var isShipAlive = true
    fun createBattleField() {
        for (item in amountOfShip) {

            if (item is OneShip) {
                for (coord in item.coordinates) {
                    if (coord.row >= selfSeaField.size || coord.column >= selfSeaField.size) {
                        selfSeaField = Array(4) { Array(4) { 0 } }
                        return println(
                            "Индекс координат больше чем макс длина поля ${selfSeaField.size - 1} \n" +
                                    "Счет начинается с 0 до ${selfSeaField.size - 1}"
                        )
                    }
                    if (coord.row < 0 || coord.column < 0) {
                        selfSeaField = Array(4) { Array(4) { 0 } }
                        return println("Индекс координат меньше чем мин значение 0")
                    }

                    if (selfSeaField[coord.row][coord.column] == 0) {
                        selfSeaField[coord.row][coord.column] = 1
                    } else {
                        println("На данной позиции нельзя поставить корабль")
                        selfSeaField = Array(4) { Array(4) { 0 } }
                        return println("Некорректные координаты для кораблей, необходимо обновить значение кораблей")
                    }
                }
            } else if (item is TwoShip) {
                for (coord in item.coordinates) {
                    if (selfSeaField[coord.row][coord.column] == 0) {
                        selfSeaField[coord.row][coord.column] = 2
                    } else {
                        println("На данной позиции нельзя поставить корабль")
                        selfSeaField = Array(4) { Array(4) { 0 } }
                        return println("Некорректные координаты для кораблей, необходимо обновить значение кораблей")
                    }
                }
            }

        }

    }

    fun allShipIsAlive(): Boolean {
        var temp = 0

        for (item in selfSeaField) {
            temp += item.sum()
        }

        isShipAlive = temp != 0
        return isShipAlive
    }

    fun shootToMyShip(row: Int, column: Int) {

        // возможно стоит использовать when, но пока так
        if (selfSeaField[row][column] == 0) {
            return println("Промазал")
        }
        if (selfSeaField[row][column] == 1) {
            selfSeaField[row][column] = 0
            println("Убил")
        }
        if (selfSeaField[row][column] == 2) {
            selfSeaField[row][column] = 0

            val coordinates = Coordinates(row, column)
            for (item in amountOfShip) {
                if (item.containsRowAndColumn(coordinates)) {
                    item.decAvailableSizeOfShip()
                    return if (item.checkShipIsAlive()) {
                        println("Попал")
                    } else {
                        println("Убил")
                    }

                }
            }
        }
    }

    fun printSea() {
        for (row in selfSeaField) {

            for (cell in row) {
                print("$cell \t")
            }
            println()
        }
    }
}


fun main() {
    println("Игра началась:")


    val setOfShipsPlayer1 = listOf(
        OneShip(listOf(Coordinates(1, 1))),
        TwoShip(
            listOf(
                Coordinates(2, 2),
                Coordinates(2, 3)
            )
        ), OneShip(listOf(Coordinates(3, 0)))
    )

    val setOfShipsPlayer2 = listOf(
        OneShip(listOf(Coordinates(0, 3))),
        TwoShip(
            listOf(
                Coordinates(2, 0),
                Coordinates(2, 1)
            )
        ), OneShip(listOf(Coordinates(2, 3)))
    )

    val player1 = Player("Никита", setOfShipsPlayer1)
    val player2 = Player("Станислав", setOfShipsPlayer2)
    player1.createBattleField()
    player2.createBattleField()

    var playersTurn = true


    while (player1.allShipIsAlive() && player2.allShipIsAlive()) {
        if (playersTurn) {
            println("Игрок ${player1.name} - Введите координаты:")
            val (row, column) = readLine()!!.split(" ")
            val i = row.toInt()
            val j = column.toInt()
            println("Игрок ${player1.name} стреляет")

            player2.shootToMyShip(i, j)
            if (!player2.allShipIsAlive()) {
                println("Победил игрок - ${player1.name}")
            }

            playersTurn = false
        } else {
            println("Игрок ${player2.name} - Введите координаты:")
            val (row, column) = readLine()!!.split(" ")
            val i = row.toInt()
            val j = column.toInt()

            println("Игрок ${player2.name} стреляет")

            player1.shootToMyShip(i, j)
            if (!player1.allShipIsAlive()) {
                println("Победил игрок - ${player2.name}")
            }
            playersTurn = true
        }

    }

}