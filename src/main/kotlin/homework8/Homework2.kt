package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    val resultVegetables = calculateVegetables(userCart, vegetableSet)
    val resultPrice = calculatePrice(userCart, discountSet, discountValue, prices)
    println("Количество овощей в корзине $resultVegetables")
    println("Сумма товаров в корзине $resultPrice")
}

fun calculateVegetables(userCart: Map<String, Int>, vegetableSet: Set<String>): Int {
    val userProduct = userCart.keys.toMutableSet()
    var result = 0
    userProduct.retainAll(vegetableSet)
    for (item in userProduct) {
        result += userCart[item]!!
    }
    return result
}

fun calculatePrice(
    userCart: Map<String, Int>,
    discountSet: Set<String>,
    discountValue: Double,
    prices: Map<String, Double>
): Double {

    var result = 0.0

    for (item in userCart.keys) {
        if (discountSet.contains(item)) {
            result += prices[item]!! * (1 - discountValue) * userCart[item]!!
        } else {
            result += prices[item]!! * userCart[item]!!
        }
    }
    return result
}

