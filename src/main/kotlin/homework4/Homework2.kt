package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var counterExcellent = 0
    var counterGood = 0
    var counterSatisfactory = 0
    var counterBad = 0

    for(item in marks){
        when(item){
            2 -> counterBad++
            3 -> counterSatisfactory++
            4 -> counterGood++
            5 -> counterExcellent++
        }
    }

    println("Отличников - ${(counterExcellent.toDouble()/marks.size) * 100.0}%")
    println("Хорошистов - ${(counterGood.toDouble()/marks.size) * 100.0}%")
    println("Троечников - ${(counterSatisfactory.toDouble()/marks.size) * 100.0}%")
    println("Двоечников - ${(counterBad.toDouble()/marks.size) * 100.0}%")
}