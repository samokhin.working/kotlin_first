package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var counterOddNumber = 0
    var counterEvenNumber = 0

    for ( item in myArray){
        if (item % 2 == 0){
            counterEvenNumber++
        }
        else{
            counterOddNumber++
        }
    }

    println("Капитан Джо получит $counterEvenNumber")
    println("Команда получит $counterOddNumber")

}
