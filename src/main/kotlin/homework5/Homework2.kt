package homework5

fun main() {
    val digit = readLine()!!.toInt()

    println(revertDigit(digit))
}

fun revertDigit(digit: Int): Int {
    var result = 0
    var temp = digit

    while (temp >= 10) {
        result = result * 10 + temp % 10
        temp /= 10
    }

    result = result * 10 + temp

    return result
}