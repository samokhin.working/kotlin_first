package homework7

fun main() {

    println("Введите логин")
    val login = readLine()!!

    println("Введите пароль")
    val password = readLine()!!

    println("Повторите пароль")
    val passwordConfirmation = readLine()!!
    val user1 = authFunction(login, password, passwordConfirmation)

    println("Логин ${user1.login} - прошел успешно с паролем ${user1.password}")
}

fun authFunction(login: String, password: String, passwordConfirmation: String): User {
    if (login.length > 20) throw WrongLoginException("В логине не должно быть более 20 символов")
    if (password.length < 10) throw WrongPasswordException("В пароле не должно быть менее 10 символов")
    if (password != passwordConfirmation) throw Exception("Пароль и подтверждение пароля должны совпадать")

    return User(login, password)
}

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)

class User(val login: String, val password: String)
