package homework1

fun main() {
    val valInt = 18500
    val valShort: Short = 200
    val valByte: Byte = 50
    val valLong: Long = 3000000000
    val valDouble = 0.666666667
    val valFloat = 0.5f
    val valString = "Что-то авторское!"

    println("\"Заказ - \'$valInt мл лимонада\' готов!\"")
    println("\"Заказ - \'$valShort мл пина колады\' готов!\"")
    println("\"Заказ - \'$valByte мл виски\' готов!\"")
    println("\"Заказ - \'$valLong капель фреша\' готов!\"")
    println("\"Заказ - \'$valFloat литра колы\' готов!\"")
    println("\"Заказ - \'$valDouble литра эля\' готов!\"")
    println("\"Заказ - \'$valString\' готов!\"")

}