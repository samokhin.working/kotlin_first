package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    val resultList = deleteDuplicates(sourceList)
    println(resultList)
}

fun deleteDuplicates(list: List<Int>): List<Int> {
    return list.toSet().toList()
}
