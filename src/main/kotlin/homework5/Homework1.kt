package homework5

class Lion(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("meat", "fish")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Лев наелся")
        } else {
            println("Лев голоден")
        }
    }
}

class Tiger(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("meat", "fish")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Тигр наелся")
        } else {
            println("Тигр голоден")
        }
    }
}

class Hippo(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("grass", "fruit", "vegetables")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Бегемот наелся")
        } else {
            println("Бегемот голоден")
        }
    }
}

class Wolf(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("meat", "carrion")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Волк наелся")
        } else {
            println("Волк голоден")
        }
    }
}

class Giraffe(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("grass", "fruit")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Жираф наелся")
        } else {
            println("Жираф голоден")
        }
    }
}

class Elephant(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("grass", "fruit")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Слон наелся")
        } else {
            println("Слон голоден")
        }
    }
}

class Chimpanzee(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("vegetables", "fruit")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Шимпанзе наелась")
        } else {
            println("Шимпанзе голодна")
        }
    }
}

class Gorilla(val name: String, val height: Int, val weight: Int) {
    var fullness = 0
    val foodPreferences = arrayOf("vegetables", "fruit")

    fun eat(food: String) {
        for (item in foodPreferences) {
            if (food == item) {
                fullness++
                break
            }
        }
        if (fullness > 0) {
            println("Горилла наелась")
        } else {
            println("Горилла голодна")
        }
    }
}

fun main() {
    val lion = Lion("Zubo", 2, 50)
    val tiger = Tiger("Drako", 2, 79)
    val hippo = Hippo("Djoni", 3, 120)
    val giraffe = Giraffe("Bobi", 3, 56)
    val wolf = Wolf("Chernish", 1, 50)
    val elephant = Elephant("Nosatik", 1, 70)
    val chimpanzee = Chimpanzee("Kristi", 1, 40)
    val gorilla = Gorilla("Lucee", 2, 90)

    val food = "vegetables"

    lion.eat(food)
    tiger.eat(food)
    hippo.eat(food)
    giraffe.eat(food)
    wolf.eat(food)
    elephant.eat(food)
    chimpanzee.eat(food)
    gorilla.eat(food)
}